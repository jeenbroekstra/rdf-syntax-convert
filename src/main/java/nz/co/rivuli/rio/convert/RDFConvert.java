package nz.co.rivuli.rio.convert;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.RDFWriter;
import org.openrdf.rio.Rio;
import org.openrdf.rio.UnsupportedRDFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RDFConvert {

	private static Logger logger = LoggerFactory.getLogger(RDFConvert.class);

	public static void main(String[] args) {
		Options options = new Options();

		Option helpOption = new Option("h", "help", false, "Print this help");

		Option inputFormatOption = new Option(
				"i",
				"input",
				true,
				"The RDF syntax format of the input file. Valid values are 'RDF/XML', 'N-Quads', 'N-Triples', 'N3', 'Turtle', 'TriG', 'TriX', 'RDF/JSON', 'JSON-LD' and 'BinaryRDF'. If not specified, the input syntax format will be automatically determined based on the extension.");

		Option outputFormatOption = new Option(
				"o",
				"output",
				true,
				"The RDF syntax format to which the file should be converted. Valid values are 'RDF/XML' (the default), 'N-Quads', 'N-Triples', 'N3', 'Turtle', 'TriG', 'TriX', 'RDF/JSON', 'JSON-LD' and 'BinaryRDF'.");

		options.addOption(helpOption);
		options.addOption(inputFormatOption);
		options.addOption(outputFormatOption);

		CommandLineParser argsParser = new PosixParser();

		try {
			CommandLine commandLine = argsParser.parse(options, args);
			if (commandLine.hasOption(helpOption.getOpt())) {
				printUsage(options);
				System.exit(0);
			}

			String inputFormatName = commandLine
					.getOptionValue(inputFormatOption.getOpt());

			String outputFormatName = commandLine
					.getOptionValue(outputFormatOption.getOpt());

			String[] otherArgs = commandLine.getArgs();

			if (otherArgs.length > 2 || otherArgs.length < 1) {
				printUsage(options);
				System.exit(0);
			}

			String inputFilePath = otherArgs[0];
			String outputFilePath = null;
			if (otherArgs.length == 2) {
				outputFilePath = otherArgs[1];
			}

			// Determine input format.
			RDFFormat inputFormat = null;
			if (inputFormatName != null) {
				inputFormat = RDFFormat.valueOf(inputFormatName);
			} else if (inputFilePath != null) {
				inputFormat = RDFFormat.forFileName(inputFilePath);
			}

			if (inputFormat == null) {
				logger.error("Could not determine input file format");
				System.exit(1);
			}

			// Determine output format.
			RDFFormat outputFormat = null;
			if (outputFormatName != null) {
				outputFormat = RDFFormat.valueOf(outputFormatName);
			} else if (outputFilePath != null) {
				outputFormat = RDFFormat.forFileName(outputFilePath);
			}

			if (outputFormat == null) {
				logger.warn("Could not determine output file format, defaulting to RDF/XML");
				outputFormat = RDFFormat.RDFXML;
			}

			RDFParser rdfParser = Rio.createParser(inputFormat);

			File inputFile = new File(inputFilePath);

			OutputStream outputStream = null;
			if (outputFilePath != null) {
				outputStream = new FileOutputStream(outputFilePath);
			} else {
				outputStream = System.out;
			}

			RDFWriter rdfWriter = null;
			try {
				rdfWriter = Rio.createWriter(outputFormat, outputStream);
			} catch (UnsupportedRDFormatException e1) {
				logger.error("no writer available for RDF Format "
						+ outputFormat);
				System.exit(1);
			}
			rdfParser.setRDFHandler(rdfWriter);

			try {
				rdfParser.parse(new FileInputStream(inputFile), inputFile
						.toURI().toString());
			} catch (RDFParseException e) {
				logger.error("Syntax error in input file:");
				logger.error(e.getMessage());

			} catch (RDFHandlerException e) {
				logger.error("Error while writing to file: " + e.getMessage());
			} catch (IOException e) {
				logger.error("I/O error: " + e.getMessage());
			}

		} catch (ParseException e) {
			logger.error(e.getMessage());
			System.exit(1);
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
			System.exit(1);
		}
	}

	private static void printUsage(Options options) {
		System.out
				.println("RDFConvert, a command line tool for RDF syntax conversion.");
		System.out.println("Developed by Jeen Broekstra");
		System.out.println();

		HelpFormatter formatter = new HelpFormatter();
		formatter.setWidth(80);
		formatter.printHelp(
				"rdfconvert [OPTIONS] <sourcefile> [<destinationfile>]",
				options);
		System.out
				.println("If <destinationfile> is not specified, the output will be sent to STDOUT.");
		System.out.println();
		System.out
				.println("Licensed under a BSD style license. See LICENSE.txt for details.");
		System.out
				.println("Copyright 2011-2014 Rivuli Development - http://www.rivuli-development.com/");
	}

}
